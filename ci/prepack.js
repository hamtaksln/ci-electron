const fs = require('fs');
const pkg = require('../package.json');

var app;
try {
    app = require('../src/package.json');
}
catch(e) {
    if (e.code === 'MODULE_NOT_FOUND') {
        process.exit();
    }
    throw e;
}

//
// util
//
function deleteFolderRecursive(path) {
    if (fs.existsSync(path)) 
    {
        fs.readdirSync(path).forEach(function(file, index)
        {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) 
            { // recurse
                deleteFolderRecursive(curPath);
            } 
            else 
            { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

//
// main
//
// clean up package folder
deleteFolderRecursive('packages');

// update electron package.json with the one from the app
pkg.name = app.name;
pkg.version = app.version;
pkg.author = app.author;
if (app.description)
    pkg.description = app.description;
if (app.productName)
    pkg.productName = app.productName;
if (app.repository)
    pkg.repository = app.repository;

if (app.dependencies) {
    pkg.dependencies = app.dependencies;
}

for (var p in app.build) {
    pkg.build[p] = app.build[p];
}

var pkgStr = JSON.stringify(pkg, null, '    ');
//console.log(pkgStr);
fs.writeFileSync(__dirname + '/../package.json', pkgStr);